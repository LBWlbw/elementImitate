
## 介绍
一些小玩意····
技术栈 **vue3, typescript, vite, vite-plugin-md, tailwindcss, scss**

## 已经实现组件

**基础组件:**
`button 按钮` `layout 布局` `container 容器` `icon 图标`<br>
**form 表单组件:**
`radio 单选框` `switch 开关` `input 输入框`<br>
**data 数据展示:**
`badge 角标` <br>
**反馈组件:**
`message 消息提示` `message box 消息弹出框` `drawer 抽屉` <br>

## 版本

> **版本：1.0.2**<br>
> 更新内容：修复环境doc下打包错误
