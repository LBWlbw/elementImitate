# 快速开始

本节将介绍如何在项目中使用 elementimitate。

## 用法

## 完整引入

如果你对打包后的文件大小不是很在乎，那么使用完整导入会更方便。

```js
import { createApp } from 'vue';
import App from './App.vue';
// 引入组件
import elementImitate from 'elementimitate';
// 引入图标 没有下载需要npm下载
import * as ElementPlusIconsVue from '@element-plus/icons-vue';
// 引入样式
import 'elementImitate/element-imitate/style.css';
// 注册组件
const app = createApp(App).use(elementImitate);
// 注册图标
for (const [key, component] of Object.entries(ElementPlusIconsVue)) {
	app.component(key, component);
}
app.mount('#app');
```

> **Tip**<br>
> 注意：umd 版本无法正常工作，es 版本才可以，请使用 es 版本，即 `element-imitate.es.js`。

> **Tip**<br>
> 如果你使用的是直接在代码仓库下载的方式，直接引入对应文件即可(`es.js`和`css`)
