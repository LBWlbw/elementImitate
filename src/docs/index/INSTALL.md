# 安装

## 环境支持

css 做过前缀兼容，对不同浏览器支持度都挺高，js 就没有专门去下那些 Polyfill 了。具体没有测试过，主流浏览器应该都是能用的，如果遇到问题，请自行添加 Polyfill。<br>
由于 Vue 3 不再支持 IE11，所以这个 vue3 开发的 ui 框架也不再支持 IE 浏览器。

|           |              |             |             |
| --------- | ------------ | ----------- | ----------- |
| Edge ≥ 79 | Firefox ≥ 78 | Chrome ≥ 64 | Safari ≥ 12 |

## 版本

目前还处于快速开发迭代中。

## 直接下载使用

项目跟目录下的 `elementImitate` 文件夹就是打包生成的插件,可以直接 copy 到项目中使用。
[项目仓库](https://gitee.com/LBWlbw/elementImitate/tree/master)

## 使用包管理器

```
# NPM
$ npm install elementimitate --save
```
