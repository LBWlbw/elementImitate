import { RouteRecordRaw } from 'vue-router';

// 数据展示组件路由
export const dataShowComponent: RouteRecordRaw[] = [
	{
		path: 'badge',
		meta: { title: 'Badge 角标' },
		component: () => import('../../docs/badge/README.md')
	}
];
