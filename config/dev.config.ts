import { defineConfig } from 'vite';
import baseConfig from './base.config';

export default defineConfig({
	...baseConfig,
	mode: 'development',
	// clearScreen: false, // 不会清空上一次控制台打印信息
	build: {
		sourcemap: false // 生产环境生成 source map 文件
	},
	server: {
		host: '0.0.0.0',
		port: 5000,
		open: true,
		hmr: {
			overlay: false
		},
		strictPort: false,
		https: false
	}
});
