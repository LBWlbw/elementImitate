import { resolve } from 'path';
import { defineConfig } from 'vite';
// import vue from '@vitejs/plugin-vue'
import baseConfig from './base.config'; // 主要用于alias文件路径别名

export default defineConfig({
	...baseConfig,
	// 打包配置
	build: {
		sourcemap: false, // 不开启镜像
		outDir: 'elementImitate',
		assetsInlineLimit: 8192, // 小于 8kb 的导入或引用资源将内联为 base64 编码
		terserOptions: {
			// 生产环境移除console
			compress: {
				drop_console: true,
				drop_debugger: true
			}
		},
		lib: {
			entry: resolve(process.cwd(), './packages/components/index.ts'), // 设置入口文件
			name: 'elementImitate', // 起个名字，安装、引入用
			fileName: format => `element-imitate.${format}.js` // 打包后的文件名
		},
		rollupOptions: {
			// 确保外部化处理那些你不想打包进库的依赖
			external: ['vue', 'tailwindcss', '@element-plus/icons-vue'],
			output: {
				// 在 UMD 构建模式下为这些外部化的依赖提供一个全局变量
				globals: {
					vue: 'Vue',
					tailwindcss: 'tailwindcss',
					'@element-plus/icons-vue': '@element-plus/icons-vue'
				}
			}
		}
	}
});
