const copydir = require('copy-dir');
/**
 *  copy-dir脚本
 *
 *  打包后自己复制到对应目录下
 *
 *  命令：node ./config/copyDocs.js
 *
 */
copydir.sync(
	`${process.cwd()}/src/docs`,
	`${process.cwd()}/dist/docs`,
	{
		utimes: true,
		mode: true,
		cover: true
	},
	function (err) {
		if (err) throw err;
		console.log('done');
	}
);
