import { App } from 'vue';
import BlRadio from './src/bl-radio.vue';

export default {
    install(app: App) {
        app.component('BlRadio', BlRadio);
    }
};

export { BlRadio };
