import { App } from 'vue';
import BlIcon from './src/bl-icon.vue';

export default {
    install(app: App) {
        app.component('BlIcon', BlIcon);
    }
};

export { BlIcon };
