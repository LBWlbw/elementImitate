import { App } from 'vue';
import BlBadge from './src/bl-badge.vue';

export default {
	install(app: App) {
		app.component('BlBadge', BlBadge);
	}
};

export { BlBadge };
