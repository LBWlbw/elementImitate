import { App } from 'vue';
import BlDrawer from './src/bl-drawer.vue';

export default {
    install(app: App) {
        app.component('BlDrawer', BlDrawer);
    }
};

export { BlDrawer };
