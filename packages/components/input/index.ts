import { App } from 'vue';
import BlInput from './src/bl-input.vue';

export default {
    install(app: App) {
        app.component('BlInput', BlInput);
    }
};

export { BlInput };
