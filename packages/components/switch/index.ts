import { App } from 'vue';
import BlSwitch from './src/bl-switch.vue';

export default {
    install(app: App) {
        app.component('BlSwitch', BlSwitch);
    }
};

export { BlSwitch };
