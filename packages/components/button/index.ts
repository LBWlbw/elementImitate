import { App } from 'vue';
import BlButton from './src/bl-button.vue';
import BlButtonGroup from './src/bl-button-group.vue';

export default {
	install(app: App) {
		app.component('BlButton', BlButton);
		app.component('BlButtonGroup', BlButtonGroup);
	}
};
export { BlButtonGroup, BlButton };
