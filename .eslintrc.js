module.exports = {
	env: {
		browser: true,
		es2021: true,
		'vue/setup-compiler-macros': true
	},
	parser: 'vue-eslint-parser',
	parserOptions: {
		ecmaVersion: 12,
		parser: '@typescript-eslint/parser',
		sourceType: 'module'
	},
	plugins: ['vue', '@typescript-eslint'],
	extends: [
		'airbnb-base',
		'eslint:recommended',
		'plugin:vue/vue3-recommended',
		'plugin:prettier/recommended',
		'@vue/eslint-config-typescript/recommended',
		'@vue/eslint-config-prettier',
		'@vue/typescript/recommended'
	],
	rules: {
		'no-console': 'off',
		'global-require': 'off',
		'import/extensions': 'off',
		'import/no-extraneous-dependencies': 'off',
		'import/no-unresolved': 'off',
		'import/prefer-default-export': 'off',
		'max-classes-per-file': 'off',
		'no-restricted-syntax': 'off',
		'no-unused-expressions': 'off',
		'consistent-return': 'off',
		'no-param-reassign': [
			'error',
			{
				props: true,
				ignorePropertyModificationsFor: ['state', 'acc', 'e']
			}
		],
		'no-plusplus': 'off',
		'no-shadow': 'off',
		'no-unused-vars': 'off',
		'no-use-before-define': 'off',
		'guard-for-in': 0,
		'vue/require-default-prop': 'off',
		'vue/multi-word-component-names': [
			'error',
			{
				ignores: ['index']
			}
		],
		'@typescript-eslint/ban-types': [
			'error',
			{
				types: {
					'{}': {
						message: 'Use object instead',
						fixWith: 'object'
					}
				}
			}
		],
		'@typescript-eslint/no-empty-interface': [
			'error',
			{
				allowSingleExtends: true
			}
		],
		'@typescript-eslint/no-explicit-any': 'off',
		'@typescript-eslint/no-non-null-assertion': 'off',
		'@typescript-eslint/no-shadow': 'error',
		'@typescript-eslint/no-unused-vars': 'off',
		'@typescript-eslint/no-use-before-define': 'off',
		'@typescript-eslint/ban-ts-comment': 'off',
		'@typescript-eslint/no-var-requires': 'off'
	},
	overrides: [
		{
			files: ['*.vue'],
			rules: {
				'no-undef': 'off'
			}
		},
		{
			files: ['*.html'],
			rules: {
				'vue/comment-directive': 'off'
			}
		}
	]
};
